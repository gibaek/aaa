package com.company;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConvertJsTo4 {

    private Common c = new Common();

    private String[] formatting(ArrayList<String> in) {
        StringBuilder sb = new StringBuilder();

        for (String line: in) {
            sb.append(line);
        }
        String temp = sb.toString();
        temp = temp.replaceAll("\\s+"," ");
        temp = temp.replaceAll("> <","><");
        temp = temp.replaceAll("><",">\r\n<");
        String lineArray [] = temp.split("\r\n");

//        System.out.println(temp);
        return lineArray;
    }

    public void convert() {
        ArrayList<String> filePath = c.subDirList("./in");

        for (int i = 0; i < filePath.size(); i++) {
            ArrayList<String> in = c.readFile(filePath.get(i));
            ArrayList<String> out = new ArrayList<>();
            String lineArray[] = formatting(in);

            for (int j = 0; j < lineArray.length; j++) {
                String line = lineArray[j];

//                if(line.contains("ism-tooltip")) {
//                    Pattern p = Pattern.compile("(?<=ism-tooltip=\").+?(?=\")");
//                    //Pattern p = Pattern.compile("(?<=ism-tooltip=\").+(?=\")");
//                    Matcher m = p.matcher(line);
//                    String t = "";
//                    while(m.find()){
//                        t = m.group(0);
//                    }
//                    System.out.println(t);
//                    if(t.length()>0){
//                        String r = "{{'"+t+"' | translate}}";
//                        System.out.println("r " + r);
//                        line = line.replace(t,r);
//                        line = line.replace("ism-tooltip", "matTooltip");
//                    }
//
//                }
                if(line.contains("ng-bind")) {
                    //System.out.println("line : "+line);
                    Pattern p = Pattern.compile("(?<=ng-bind=\").+?(?=\")");
                    Matcher m = p.matcher(line);
                    String t = "";
                    while(m.find()){
                        t = m.group(0);
                    }
                    if(t.length()>0){
                        //System.out.println(t);
                        line = line.replaceAll("ng-bind=\""+t+"\"","");
//                        line = line.replaceAll("ng-bind=\"\"","");
                        line = line+"{{ "+t+" }}";
                    }
                }

                if(line.contains("translate")) {
//                    System.out.println("line : "+line);
                    Pattern p = Pattern.compile("(?<=translate=\").+?(?=\")");
                    Matcher m = p.matcher(line);
                    String t = "";
                    while(m.find()){
                        t = m.group(0);
                    }
                    try{
                        if(t.length()>0){
                            //System.out.println(t);
                            line = line.replaceAll(t,"");
                            line = line.replaceAll("=\"\"","");
                            line = line+t;
                        }
                    }catch (Exception e){
                        //e.printStackTrace();
                        System.out.println("err file: " + filePath.get(i));
                        System.out.println("err line: " + line);

                    }

                }

                line = line.replace("ng-show", "*ngIf");
                line = line.replace("ng-if", "*ngIf");
                line = line.replace("ng-cloak", "*ngIf");
                line = line.replace("==", "===");
                line = line.replace("ng-click", "(click)");
//                line = line.replace("ism", "nxc");
                line = line.replace("ng-switch-when=\"complete\"", "*ngSwitchCase=\"'complete'\"");
                line = line.replace("ng-switch-when=\"progress\"", "*ngSwitchCase=\"'progress'\"");
                line = line.replace("ng-switch-when=", "*ngSwitchCase=");

                line = line.replace("ng-switch-default", "*ngSwitchDefault");
                line = line.replace("ng-switch", "[ngSwitch]");
//                line = line.replace("ng-bind", "[innerHTML]");
//                line = line.replace("ng-model", "[(ngModel)]");
                line = line.replace("ng-change", "(change)");
                line = line.replace("ng-style", "[ngStyle]");
                line = line.replace("ng-class", "[ngClass]");
                line = line.replace("ng-disabled", "[disabled]");

                if(line.equals("</span>") || line.equals("</line>")|| line.equals("</label>")){
                    String temp = out.get(out.size()-1);
                    temp = temp + line;
                    out.set(out.size()-1, temp);
                }else {
                    out.add(line);
                }
            }
            c.writeFile(out, filePath.get(i).replace("in/", "res/"));

        }
    }

}
