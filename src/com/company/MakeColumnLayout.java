package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;

public class MakeColumnLayout {

    ArrayList<String> filePath = new ArrayList<>();
    ArrayList<String> failedFilePath = new ArrayList<>();

    private boolean ck;

    public void makeCol(String source, boolean ck) {
        this.ck = ck;
        subDirList(source);
        for (int i = 0; i < filePath.size(); i++) {
            readJson(filePath.get(i));
        }
    }

    private void readJson(String path) {

        ArrayList<DTO> params = new ArrayList<>();
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path));
            JSONObject jsonObject = (JSONObject) obj;

            JSONObject res = (JSONObject) jsonObject.get("res");
            JSONArray cols = (JSONArray) res.get("columns");

            for (int i = 0; i < cols.size() ; i++) {
                JSONObject col = (JSONObject) cols.get(i);
                long show = 0;
                try {
                    show = (long) col.get("show");
                } catch (Exception e) {
                    System.out.println("Class Cast Err" + path);
                }

                if(show == 1){
                    DTO param = new DTO();
                    String binding = (String) col.get("key");
                    String header = (String) col.get("displayName");
                    String align = (String) col.get("textAlign");
                    String width = (String) col.get("width");

                    if(align.length() == 0){
                        align = "left";
                    }

                    param.align = align;
                    param.width = width.replace("px", "");
                    param.binding = binding;
                    param.header = header;
                    params.add(param);
                }

            }
            writeFile(path, params);

        } catch (FileNotFoundException e) {
//            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } catch (ParseException e) {
            failedFilePath.add(path);
        }


    }

    private void writeFile(String path, ArrayList<DTO> params) {
        String[] temp = path.split("/");
        String name = temp[temp.length-1];
        name = name.replace("_columns.json","");
        name = name.replace("_columns_info.json","");
        name = name.replace(".json","");
        name = name.replace("_columns","");

        path = path.substring(0,path.lastIndexOf("/"));
        path = path.replace("dummy-user", "res");

        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        try {
            File f = new File(path+"/"+name+".ts");
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);

            bw.write("export const "+name + " = {");
            bw.newLine();
            bw.write("    ColumnLayout: [");
            bw.newLine();

            if(ck) {
                bw.write("        {\n" +
                        "            binding: 'ck',\n" +
                        "            header: 'ck',\n" +
                        "            width: 38,\n" +
                        "            minWidth: 38,\n" +
                        "            align: 'center',\n" +
                        "            format: 'N0',\n" +
                        "            dataType: 1,\n" +
                        "            visible: true,\n" +
                        "            cellType: 'ck'\n" +
                        "        },");
                bw.newLine();
            }

            for (int i = 0; i < params.size(); i++) {
                DTO p = params.get(i);
                bw.write("        {");
                bw.newLine();
                bw.write("            binding: '"+p.binding+"',");
                bw.newLine();
                bw.write("            header: '"+p.header+"',");
                bw.newLine();
                bw.write("            width: "+p.width+",");
                bw.newLine();
                bw.write("            minWidth: 80,");
                bw.newLine();
                bw.write("            align: '"+p.align+"',");
                bw.newLine();
                bw.write("            format: 'N0',");
                bw.newLine();
                bw.write("            dataType: 1,");
                bw.newLine();
                bw.write("            visible: true,");
                bw.newLine();
                bw.write("        },");
                bw.newLine();
            }
            bw.write("    ]");
            bw.newLine();
            bw.write("};");
            bw.newLine();
            bw.flush();
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void subDirList(String source) {

        File dir = new File(source);
        File[] fileList = dir.listFiles();
        try {
            for (int i = 0; i < fileList.length; i++) {
                File file = fileList[i];
                if (file.isFile()) {
                    if (file.getName().contains("columns")) {
//                        System.out.println(file.getCanonicalPath());
                        filePath.add(file.getCanonicalPath());
                    }
                } else if (file.isDirectory()) {
                    subDirList(file.getCanonicalPath().toString());
                }
            }
        } catch (IOException e) {
        }
    }
}

class DTO {
    public String binding;
    public String header;
    public String width;
    public String align;
}