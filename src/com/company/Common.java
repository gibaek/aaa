package com.company;

import java.io.*;
import java.util.ArrayList;

public class Common {

    public ArrayList<String> subDirList(String source) {

        ArrayList<String> filePath = new ArrayList<>();
        File dir = new File(source);
        File[] fileList = dir.listFiles();
        try {
            for (int i = 0; i < fileList.length; i++) {
                File file = fileList[i];
                if (file.isFile()) {
                    if (file.getName().contains("html")) {
//                        System.out.println(file.getCanonicalPath());
                        filePath.add(file.getCanonicalPath());
                    }
                } else if (file.isDirectory()) {
                    subDirList(file.getCanonicalPath().toString());
                }
            }
        } catch (IOException e) {
        }
        return filePath;
    }

    public ArrayList<String> readFile(String source){
        ArrayList<String> in = new ArrayList<>();
        try{
            File f = new File(source);
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            while((line = br.readLine())!=null) {
                in.add(line);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return in;
    }

    public void writeFile(ArrayList<String> out, String path) {

        try {
            File f = new File(path);
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);

            for (int i = 0; i < out.size(); i++) {
                bw.write(out.get(i));
                bw.newLine();
            }
            bw.flush();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
